package com.fisi.sgca.comunes.entity;

import com.fisi.sgca.comunes.key.CommonKey;

import java.io.Serializable;




public class Subtema implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Attributes used for CommonKey key are:
	 * 	planID
	 * 	cursoID
	 * 	grupoID
	 * 	semana
	 * 	seccion
	 */
	private int subtemaID;
	private CommonKey key;
	private String descripcion;
	private String detalle;
	private int orden;
	private int estado;
	private double calificacion;



	public Subtema() {
	}

	public Subtema(int subtemaID, CommonKey key, String descripcion, String detalle, int orden, int estado,
			double calificacion) {
		super();
		this.subtemaID = subtemaID;
		this.key = key;
		this.descripcion = descripcion;
		this.detalle = detalle;
		this.orden = orden;
		this.estado = estado;
		this.calificacion = calificacion;
	}



	public int getSubtemaID() {
		return subtemaID;
	}

	public void setSubtemaID(int subtemaID) {
		this.subtemaID = subtemaID;
	}

	public CommonKey getKey() {
		return key;
	}

	public void setKey(CommonKey key) {
		this.key = key;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public String toString() {
		return "Subtema{" +
				"subtemaID=" + subtemaID +
				", key=" + key.toString() +
				", descripcion='" + descripcion + '\'' +
				", detalle='" + detalle + '\'' +
				", orden=" + orden +
				", realizado=" + estado +
				'}';
	}
}
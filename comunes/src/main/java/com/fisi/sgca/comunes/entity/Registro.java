package com.fisi.sgca.comunes.entity;

import java.io.Serializable;
import java.util.Date;



public class Registro implements Serializable {
	private static final long serialVersionUID = 1L;

	private int registroID;
	private String curso;
	private Date fecharegistro;
	private String grupo;
	private String plan;
	private String seccion;
	private String usuario;

	public Registro() {
	}

	public Registro(String curso, String grupo, String plan) {
		this.curso = curso;
		this.grupo = grupo;
		this.plan = plan;
	}

	

	public int getRegistroID() {
		return this.registroID;
	}

	public void setRegistroID(int registroID) {
		this.registroID = registroID;
	}

	public String getCurso() {
		return this.curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public Date getFecharegistro() {
		return this.fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getPlan() {
		return this.plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getSeccion() {
		return this.seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Registro{" +
				"registroID=" + registroID +
				", curso='" + curso + '\'' +
				", fecharegistro=" + fecharegistro +
				", grupo='" + grupo + '\'' +
				", plan='" + plan + '\'' +
				", seccion='" + seccion + '\'' +
				", usuario='" + usuario + '\'' +
				'}';
	}
}
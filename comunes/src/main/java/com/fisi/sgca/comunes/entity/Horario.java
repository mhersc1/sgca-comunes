package com.fisi.sgca.comunes.entity;

import com.fisi.sgca.comunes.key.CommonKey;

import java.io.Serializable;




public class Horario implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Attributes used for CommonKey key are:
	 * 	planID
	 * 	cursoID
	 * 	grupoID
	 * 	seccion
	 */
	private int horarioID;
	private CommonKey key;
	private String dia;
	private String horafin;
	private String horainicio;



	public Horario() {
	}

	public int getHorarioID() {
		return this.horarioID;
	}

	public void setHorarioID(int horarioID) {
		this.horarioID = horarioID;
	}

	public String getDia() {
		return this.dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getHorafin() {
		return this.horafin;
	}

	public void setHorafin(String horafin) {
		this.horafin = horafin;
	}

	public String getHorainicio() {
		return this.horainicio;
	}

	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}

	public CommonKey getKey() {
		return key;
	}

	public void setKey(CommonKey key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return "Horario{" +
				"horarioID=" + horarioID +
				", key=" + key.toString() +
				", dia='" + dia + '\'' +
				", horafin='" + horafin + '\'' +
				", horainicio='" + horainicio + '\'' +
				'}';
	}
}
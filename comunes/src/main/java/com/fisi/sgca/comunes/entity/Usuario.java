package com.fisi.sgca.comunes.entity;

import java.io.Serializable;

/**
 * Created by MHER on 17/06/2017.
 */
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    private String usuarioID;

    private String activo;

    private String apematerno;

    private String apepaterno;

    private String estado;

    private String nombres;

    private String perfil;

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getApematerno() {
        return apematerno;
    }

    public void setApematerno(String apematerno) {
        this.apematerno = apematerno;
    }

    public String getApepaterno() {
        return apepaterno;
    }

    public void setApepaterno(String apepaterno) {
        this.apepaterno = apepaterno;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    /**
     * Metodo para obtener el nombre completo del usuario
     * @return
     */
    public String getNombreCompleto() {
    	return getNombres() + " " + getApepaterno() + " " + getApematerno();
    }
}

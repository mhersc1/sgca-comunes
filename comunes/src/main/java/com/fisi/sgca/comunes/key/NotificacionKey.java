package com.fisi.sgca.comunes.key;

import java.util.List;

public class NotificacionKey {
	private List<String> listaCodUsuarios;
	private String mensaje;
	private List<Integer> listaCodNotificaciones;
	

	public List<String> getListaCodUsuarios() {
		return listaCodUsuarios;
	}
	public void setListaCodUsuarios(List<String> listaCodUsuarios) {
		this.listaCodUsuarios = listaCodUsuarios;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<Integer> getListaCodNotificaciones() {
		return listaCodNotificaciones;
	}
	public void setListaCodNotificaciones(List<Integer> listaCodNotificaciones) {
		this.listaCodNotificaciones = listaCodNotificaciones;
	}
	
	
}

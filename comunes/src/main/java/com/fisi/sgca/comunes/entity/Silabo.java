package com.fisi.sgca.comunes.entity;

import com.fisi.sgca.comunes.key.CommonKey;

import java.io.Serializable;


public class Silabo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * Attributes used for CommonKey key are:
     * 	planID
     * 	cursoID
     * 	grupoID
     */
    private int silaboID;
    private CommonKey key;
    private String ciclo;
    private String descripcion;
    private String detalle;

    private int avance = 0;
    private int totalSubTemas = 0;

    public Silabo() {
    }

    public int getSilaboID() {
        return this.silaboID;
    }

    public void setSilaboID(int silaboID) {
        this.silaboID = silaboID;
    }

    public String getCiclo() {
        return this.ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public CommonKey getKey() {
        return key;
    }

    public void setKey(CommonKey key) {
        this.key = key;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDetalle() {
        return this.detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getAvance() {
        return avance;
    }

    public void setAvance(int avance) {
        this.avance = avance;
    }

    public int getTotalSubTemas() {
        return totalSubTemas;
    }

    public void setTotalSubTemas(int totalSubTemas) {
        this.totalSubTemas = totalSubTemas;
    }

    @Override
    public String toString() {
        return "Silabo{" +
                "silaboID=" + silaboID +
                ", key=" + key.toString() +
                ", ciclo='" + ciclo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", detalle='" + detalle + '\'' +
                ", avance=" + avance +
                ", totalSubTemas=" + totalSubTemas +
                '}';
    }
}
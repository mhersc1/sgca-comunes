package com.fisi.sgca.comunes.entity;

import java.io.Serializable;

/**
 * Created by MHER on 17/06/2017.
 */
public class Parametro implements Serializable {


    private int parametroID;
    private String parametro;
    private String tipo;
    private String valor;

    public Parametro() {
    }

    public int getParametroID() {
        return parametroID;
    }

    public void setParametroID(int parametroID) {
        this.parametroID = parametroID;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Parametro{" +
                "parametroID=" + parametroID +
                ", parametro='" + parametro + '\'' +
                ", tipo='" + tipo + '\'' +
                ", valor='" + valor + '\'' +
                '}';
    }
}

package com.fisi.sgca.comunes.entity;

import com.fisi.sgca.comunes.key.CommonKey;

import java.io.Serializable;


public class Tema implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Attributes used for CommonKey key are:
	 * 	planID
	 * 	cursoID
	 * 	grupoID
	 * 	semana
	 */
	private int temaID;
	private CommonKey key;
	private String descripcion;
	private String detalle;
	private int orden;
	private int completado = 0;//indica si el tema esta con todos sus subtemas completado

	public Tema() {
	}

	public int getTemaID() {
		return this.temaID;
	}

	public void setTemaID(int temaID) {
		this.temaID = temaID;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDetalle() {
		return this.detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public int getOrden() {
		return this.orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public CommonKey getKey() {
		return key;
	}

	public void setKey(CommonKey key) {
		this.key = key;
	}

	public int getCompletado() {
		return completado;
	}

	public void setCompletado(int completado) {
		this.completado = completado;
	}

	@Override
	public String toString() {
		return "Tema{" +
				"temaID=" + temaID +
				", key=" + key.toString() +
				", descripcion='" + descripcion + '\'' +
				", detalle='" + detalle + '\'' +
				", orden=" + orden +
				", completado=" + completado +
				'}';
	}
}
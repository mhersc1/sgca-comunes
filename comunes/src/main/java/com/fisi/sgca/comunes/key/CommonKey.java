package com.fisi.sgca.comunes.key;

import java.io.Serializable;

/**
 * Created by MHER on 09/09/2017.
 */
public class CommonKey implements Serializable{
    private String planID;
    private String cursoID;
    private String grupoID;
    private String semana;
    private String seccion;
    private String coduser;

    public CommonKey(String planID, String cursoID, String grupoID, String semana, String seccion) {
        this.planID = planID;
        this.cursoID = cursoID;
        this.grupoID = grupoID;
        this.semana = semana;
        this.seccion = seccion;
    }

    public CommonKey(String planID, String cursoID, String grupoID, String seccion) {
        this.planID = planID;
        this.cursoID = cursoID;
        this.grupoID = grupoID;
        this.seccion = seccion;
    }

    public CommonKey(String planID, String cursoID, String grupoID) {
        this.planID = planID;
        this.cursoID = cursoID;
        this.grupoID = grupoID;
    }

    public CommonKey() {
    }

    public String getCoduser() {
        return coduser;
    }

    public void setCoduser(String coduser) {
        this.coduser = coduser;
    }

    public String getPlanID() {
        return planID;
    }

    public void setPlanID(String planID) {
        this.planID = planID;
    }

    public String getCursoID() {
        return cursoID;
    }

    public void setCursoID(String cursoID) {
        this.cursoID = cursoID;
    }

    public String getGrupoID() {
        return grupoID;
    }

    public void setGrupoID(String grupoID) {
        this.grupoID = grupoID;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    @Override
    public String toString() {
        return "CommonKey{" +
                "planID='" + planID + '\'' +
                ", cursoID='" + cursoID + '\'' +
                ", grupoID='" + grupoID + '\'' +
                ", semana='" + semana + '\'' +
                ", seccion='" + seccion + '\'' +
                ", coduser='" + coduser + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommonKey commonKey = (CommonKey) o;

        if (!planID.equals(commonKey.planID)) return false;
        if (!cursoID.equals(commonKey.cursoID)) return false;
        if (!grupoID.equals(commonKey.grupoID)) return false;
        if (semana != null ? !semana.equals(commonKey.semana) : commonKey.semana != null) return false;
        if (seccion != null ? !seccion.equals(commonKey.seccion) : commonKey.seccion != null) return false;
        return coduser != null ? coduser.equals(commonKey.coduser) : commonKey.coduser == null;
    }

    @Override
    public int hashCode() {
        int result = planID.hashCode();
        result = 31 * result + cursoID.hashCode();
        result = 31 * result + grupoID.hashCode();
        result = 31 * result + (semana != null ? semana.hashCode() : 0);
        result = 31 * result + (seccion != null ? seccion.hashCode() : 0);
        result = 31 * result + (coduser != null ? coduser.hashCode() : 0);
        return result;
    }
}

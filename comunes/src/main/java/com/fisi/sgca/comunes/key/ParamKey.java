package com.fisi.sgca.comunes.key;

import java.io.Serializable;

/**
 * Created by MHER on 06/08/2017.
 */
public class ParamKey implements Serializable {

    private String tipo;
    private String parametro;

    public ParamKey() {
    }

    public ParamKey(String tipo, String parametro) {
        this.tipo = tipo;
        this.parametro = parametro;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParamKey paramKey = (ParamKey) o;

        if (!tipo.equals(paramKey.tipo)) return false;
        return parametro.equals(paramKey.parametro);
    }

    @Override
    public int hashCode() {
        int result = tipo.hashCode();
        result = 31 * result + parametro.hashCode();
        return result;
    }
}
